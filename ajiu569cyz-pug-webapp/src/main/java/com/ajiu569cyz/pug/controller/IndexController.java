package com.ajiu569cyz.pug.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: 阿久
 * @Date: 2022/07/08/19:18
 */
@RestController
public class IndexController {
  @GetMapping("/index")
  public String index(){
      return "hello world springboot";
  }
}
