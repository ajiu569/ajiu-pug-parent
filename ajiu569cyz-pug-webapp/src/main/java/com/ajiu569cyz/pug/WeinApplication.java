package com.ajiu569cyz.pug;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: 阿久
 * @Date: 2022/07/08/19:17
 */
@SpringBootApplication
public class WeinApplication {
    public static void main(String[] args) {
        SpringApplication.run(WeinApplication.class,args);
    }
}
